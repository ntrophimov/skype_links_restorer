# -*- coding: utf-8 -*-

import time

import Skype4Py


class SkypeLinksRestorer(object):
    def __init__(self):
        self.skype = Skype4Py.Skype(Events=self)
        self.skype.FriendlyName = 'skype_links_restorer'
        self.skype.Attach()

    def AttachmentStatus(self, status):
        if status == Skype4Py.apiAttachAvailable:
            self.skype.Attach()

    def MessageStatus(self, msg, status):
        if status == Skype4Py.cmsSent:
            if msg.Chat.Type in (Skype4Py.chatTypeDialog, Skype4Py.chatTypeLegacyDialog):
                if msg.IsEditable:
                    time.sleep(1.0)
                    msg.Body = msg.Body


def main():
    skype_links_restorer = SkypeLinksRestorer()

    while True:
        time.sleep(1.0)


if __name__ == '__main__':
    main()